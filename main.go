package main

import (
	// dependencies
	"encoding/json"
	"fmt"

	// dependencies
	consulapi "github.com/hashicorp/consul/api"
)

func main() {
	// consulCfg := consulapi.DefaultConfig()
	// consulCfg.Address = "http://dev.mycompany.com:8500"

	consulCfg := &consulapi.Config{
		Address: "http://dev.mycompany.com:8500",
	}

	fmt.Printf("Addy: %v\n", consulCfg.Address)

	// create consul client
	client, err := consulapi.NewClient(consulCfg)
	if err != nil {
		panic(err)
	}

	// connect to a kv store
	kv := client.KV()

	pair, _, err := kv.Get("tests/get-me", nil)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Pair: %v\n", pair)

	fmt.Printf("Pair Enc Val: %v\n %v\n", string(pair.Value), pair.Value)

	// NOTE: Not needed - when using the library things are decoded already
	// decodeText, err := base64.StdEncoding.DecodeString(string(pair.Value))
	// if err != nil {
	// 	panic(err)
	// }
	//
	// fmt.Printf("Pair Decode Val: %v\n", decodeText)

	type response struct {
		Pie string `json:"pie"`
	}

	res := response{}
	json.Unmarshal(pair.Value, &res)
	fmt.Printf("Pair Json: %v\n", res)
	fmt.Printf("Pair Json: %v\n", res.Pie)

	fmt.Println("-------- END --------")
}
